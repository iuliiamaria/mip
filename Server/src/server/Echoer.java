package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Echoer extends Thread {
	
	private Socket socket;
	private Validate validator;
	
	public Echoer(Socket socket) {
		this.socket = socket;
		validator = new Validate();
	}
	
	/**
	 * Verify if the user name and password given are the same as the ones from the HashMap
	 * @param user
	 * @param pass
	 * @return
	 */
	public boolean validate(String user, String pass) {
		return validator.has(pass, user);
	}
	
	/**
	 * Verify if the data given to the server is the same as the data stored in the HashMap
	 * and return a message that tells us if we are granted the access or not
	 */
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter output = new PrintWriter(socket.getOutputStream(), true);
			
			String user = input.readLine();
			String pass = input.readLine();
			
			if(validate(user, pass))
				output.println("Access granted");
			else
				output.println("Access not granted");
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
