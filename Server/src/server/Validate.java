package server;

import java.util.HashMap;
import java.util.Map;

public class Validate {
	private Map<String, String> keys;
	
	//store the user name and password into a HashMap when the constructor is called
	public Validate() {
		keys = new HashMap<String, String>();
		
		keys.put("doctor", "1234");
	}
	 /**
	  * Verify if the password is found in the HashMap, 
	  * and when is found verify if the user name stored with the password is 
	  * the same as the one given to the server
	  * @param user
	  * @param pass
	  * @return
	  */
	public boolean has(String user, String pass) {
		return keys.containsKey(pass) && keys.get(pass).equals(user);
	}

}
