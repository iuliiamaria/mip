package server;

import java.io.IOException;
import java.net.ServerSocket;

public class Main {

	public static void main(String[] args) {
		ServerSocket serverSocket;
		
		//start the server
		try {
			serverSocket = new ServerSocket(5000);
			while(true) {
				new Echoer(serverSocket.accept()).start();
				System.out.println("Started server.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
