package util;

import java.io.ByteArrayInputStream;
import java.util.List;

import javafx.scene.image.Image;
import model.Animal;
import model.Programare;

public class AnimalUtil extends DatabaseUtil {
	
	/**
	 * Add an animal to the DB
	 * @param animal
	 */
	public void addToDB(Animal animal) {
		DatabaseUtil.startTransaction();
		DatabaseUtil.entityManager.persist(animal);
		DatabaseUtil.commitTransaction();
	} 
	
	/**
	 * Find an animal in the DB
	 * @param animal
	 */
	public void retrieveAnimal(Animal animal) {
		animal = DatabaseUtil.entityManager.find(Animal.class, animal.getIdAnimal());
	}
	
	/**
	 * Find an animal by id in the DB
	 * @param animal
	 */
	public int retrieveAnimalByIndex(int index) {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, index);
		return animal.getIdAnimal();
	}
	
	/**
	 * Update the name of an animal in the DB
	 * @param animal
	 */
	public void updateAnimal(Animal animal, String newName) {
		retrieveAnimal(animal);
		DatabaseUtil.startTransaction();
		animal.setName(newName);
		DatabaseUtil.commitTransaction();
	}
	
	/**
	 * Update the observations of an animal in the DB
	 * @param newObs
	 * @param id
	 */
	public void updateObservatii(String newObs, int id) {
		Animal animal = DatabaseUtil.entityManager.find(Animal.class, id);
		DatabaseUtil.startTransaction();
		animal.setObservatii(newObs);
		DatabaseUtil.commitTransaction();
	}
	
	/**
	 * Delete an animal from the DB
	 * @param animal
	 */
	public void deleteAnimal(Animal animal) {
		retrieveAnimal(animal);
		DatabaseUtil.entityManager.getTransaction().begin();
		DatabaseUtil.entityManager.remove(animal);
		DatabaseUtil.entityManager.getTransaction().commit();
	}
	
	/**
	 * Print all the the data from the table Animal
	 */
	public void printAllAnimalsFromDB() {
		@SuppressWarnings("unchecked")
		List<Animal> results = DatabaseUtil.entityManager.createNativeQuery("Select * from PetShop.Animal", Animal.class)
				.getResultList();
		for(Animal animal : results) {
			System.out.println("Animal: " + animal.getName() + " has ID: " + animal.getIdAnimal());
		}
	}
	
	/**
	 * Convert the images from byte[] type to Image type and show th
	 * @param prog
	 * @return
	 */
	public Image listImages(Programare prog) {
		Image img = null;
		@SuppressWarnings("unchecked")
		List<Animal> results = entityManager.createNativeQuery("Select * from PetShop.Animal, PetShop.Programare where PetShop.Animal.idAnimal = PetShop.Programare.idAnimal", Animal.class)
				.getResultList();
		for(Animal animal: results) {
			if(prog.getAnimal().getIdAnimal() == animal.getIdAnimal())
				img = new Image(new ByteArrayInputStream(animal.getImage()));
		}
		return img;
	}
}
