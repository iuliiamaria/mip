package util;

import java.util.List;
import java.util.Scanner;

import model.PersonalMedical;

public class PersonalMedicalUtil extends DatabaseUtil {
	
	/**
	 * Add a medical personnel to the DB
	 * @param personalMedical
	 */
	public void addToDB(PersonalMedical personalMedical) {
		DatabaseUtil.startTransaction();
		DatabaseUtil.entityManager.persist(personalMedical);
		DatabaseUtil.commitTransaction();
	} 
	
	/**
	 * Find a medical personnel in the DB
	 * @param personalMedical
	 */
	public void retrievePersonalMedical(PersonalMedical personalMedical) {
		personalMedical = entityManager.find(PersonalMedical.class, personalMedical.getIdPersonalMedical());
	}
	
	/**
	 * Update the name of a medical personnel
	 * @param personalMedical
	 */
	public void updatePersonalMedical(PersonalMedical personalMedical) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		String newJob = (scanner.next());
		retrievePersonalMedical(personalMedical);
		startTransaction();
		personalMedical.setName(newName);
		personalMedical.setFunctie(newJob);
		commitTransaction();
		scanner.close();
	}
	
	/**
	 * Delete a medical personnel from the DB
	 * @param personalMedical
	 */
	public void deletePersonalMedical(PersonalMedical personalMedical) {
		retrievePersonalMedical(personalMedical);
		entityManager.getTransaction().begin();
		entityManager.remove(personalMedical);
		entityManager.getTransaction().commit();
	}
	
	/**
	 * Print all the data from the table PersonalMedical
	 */
	public void printAllMedicalPersonnelFromDB() {
		@SuppressWarnings("unchecked")
		List<PersonalMedical> results = entityManager.createNativeQuery("Select * from PetShop.PersonalMedical", PersonalMedical.class)
				.getResultList();
		System.out.println();
		for(PersonalMedical personalMedical : results) {
			System.out.println("The medical personnel: " + personalMedical.getName() + 
					" has ID: " + personalMedical.getIdPersonalMedical() + 
					" and the job: "+ personalMedical.getFunctie());
		}
	}
}
