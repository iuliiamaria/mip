package util;

import static org.junit.Assert.*;

import org.junit.Test;

import model.Animal;

public class DBUtilTests {
	
	DatabaseGenerics<DatabaseUtil> db = DatabaseGenerics.getInstance();
	Animal animal = new Animal();
	Animal animal2 = new Animal();
	AnimalUtil dbA = new AnimalUtil();
	
	/**
	 * Test to verifiy if we can connect to the server
	 */
	@Test
	public void testConnection() {
		assertNotNull(db);
    }
	
	/**
	 * Test to verify if we can add an animal to the DB
	 */
	@Test
	public void addAnimalToDB() {
		animal.setIdAnimal(5);
    	dbA.addToDB(animal);
    	assertEquals(5, animal.getIdAnimal());
    }
	
	/**
	 * Test to verify if we can find an animal in the DB based on the id of that animal
	 */
	@Test
	public void retrieveAnimalFromBD() {
		int find = dbA.retrieveAnimalByIndex(3);
		assertEquals(3, find);
	}

	/**
	 * Test to verify if we can update the name of an animal
	 */
	@Test
	public void updateAnimalToBD() {
		animal.setIdAnimal(6);
		animal.setName("Bob");
		dbA.addToDB(animal);
		dbA.updateAnimal(animal, "Jim");
    	assertEquals("Jim", animal.getName());
	}
	
	/**
	 * Test to verify if we cand delete an animal from the DB
	 */
	@Test
	public void deleteAnimalFromBD() {
		animal.setIdAnimal(7);
		dbA.addToDB(animal);
		dbA.deleteAnimal(animal);
		assertNull(DatabaseUtil.entityManager.find(Animal.class, 7));
	}
}
