package util;

public class DatabaseGenerics<T extends DatabaseUtil> {
	
	private static DatabaseGenerics<DatabaseUtil> instance;
	
	//singleton initialization
	static {
		try {
			instance = new DatabaseGenerics<>();
		}catch(Exception e) {
			System.out.println("Exception occured in creating singleton");
		}
	}
	
	public static DatabaseGenerics<DatabaseUtil> getInstance() {
		return instance;
	}

	/**
	 * Constructor where the connection with the database is started
	 * @throws Exception
	 */
	
	private DatabaseGenerics() throws Exception {
		DatabaseUtil.setUp();
		DatabaseUtil.startTransaction();
		DatabaseUtil.commitTransaction();
	}

	/**
	 * Generic method to save data in the DB
	 * @param db
	 */
	public void addToDB(DatabaseGenerics<T> db) {
		DatabaseUtil.entityManager.persist(db);
	} 
	
	/**
	 * Stop the connection with the database
	 */
	public void closeEntity() {
		DatabaseUtil.closeEntityManager();
	}

}
