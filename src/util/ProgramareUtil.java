package util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javafx.scene.control.DatePicker;
import model.Programare;

public class ProgramareUtil extends DatabaseUtil{
	
	/**
	 * Add an appointment to the DB
	 * @param programare
	 */
	public void addToDB(Programare programare) {
		DatabaseUtil.startTransaction();
		DatabaseUtil.entityManager.persist(programare);
		DatabaseUtil.commitTransaction();
	} 
	
	/**
	 * Find an appointment in the DB
	 * @param programare
	 */
	public void retrieveProgramare(Programare programare) {
		programare = entityManager.find(Programare.class, programare.getIdProgramare());
	}
	
	public Programare retrieveProgramareByDate(LocalDate data) {
		Programare programare = entityManager.find(Programare.class, data);
		return programare;
	}
	
	/**
	 * Update the diagnostic from the appointment in the DB
	 * @param programare
	 */
	public void updateProgramare(Programare programare) {
		Scanner scanner = new Scanner(System.in);
		String newName = (scanner.next());
		retrieveProgramare(programare);
		startTransaction();
		programare.setDiagnostic(newName);
		commitTransaction();
		scanner.close();
	}
	
	/**
	 * Delete an appointment from the DB
	 * @param programare
	 */
	public void deleteProgramare(Programare programare) {
		retrieveProgramare(programare);
		entityManager.getTransaction().begin();
		entityManager.remove(programare);
		entityManager.getTransaction().commit();
	}

	/**
	 * Print all the data from the table Programare
	 */
	public void printAllAppointmentsFromDB() {
		@SuppressWarnings("unchecked")
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		System.out.println();
		for(Programare programare : results) {
			System.out.println("The appointment: " + programare.getDiagnostic() + " has ID: " 
					+ programare.getIdProgramare() + " and the date is: " 
					+ programare.getData());
		}
	}
	
	/**
	 * Print all the data and the foreign keys from the table Programare
	 */
	public void printAllAppointmentsAndForeignKeys() {
		@SuppressWarnings("unchecked")
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		System.out.println();
		for(Programare programare : results) {
			System.out.println("The appointment: " + programare.getDiagnostic() + " has ID: " 
					+ programare.getIdProgramare() + " and the date is: " 
					+ programare.getData() + " for the animal " + programare.getAnimal().getIdAnimal() 
					+ " at the medical personnel " + programare.getPersonalMedical().getIdPersonalMedical());
		}
	}
	
	/**
	 * List that contains all the appointments sorted by date
	 * @return
	 */
	public List<Programare> listAllAppointmentsSortedByDate() {
		@SuppressWarnings("unchecked")
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		Collections.sort(results, (p1, p2) -> p1.getData().compareTo(p2.getData()));
		return results;
	}
	
	/**
	 * List that contains the appointments of a certain date chosen
	 * @param date
	 * @return
	 */
	public List<Programare> listAllAppointments(DatePicker date) {
		@SuppressWarnings("unchecked")
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Programare", Programare.class)
				.getResultList();
		
		List<Programare> currentDateProg = new ArrayList<>();
		for(Programare prog : results) {
			LocalDate currentDate = date.getValue();
			Instant instant = prog.getData().toInstant();
			LocalDate progDate = instant.atZone(ZoneId.systemDefault()).toLocalDate();
			
			if(progDate.equals(currentDate) == true)
				currentDateProg.add(prog);
			else
				currentDateProg.remove(prog);
			}
		return 	currentDateProg;
		}
	
	/**
	 * List that contains all the diagnostics
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Programare>listDiagnostic() {
		return entityManager.createNativeQuery("Select PetShop.Programare.diagnostic from PetShop.Animal, PetShop.Programare where PetShop.Animal.idAnimal = PetShop.Programare.idAnimal", Programare.class)
				.getResultList();
	}
	
	/**
	 * List that contains all the dates of the appointments
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public List<LocalDate> listDates() {
		List<LocalDate> localDate = entityManager.createNativeQuery("Select * from PetShop.Animal, PetShop.Programare where PetShop.Animal.idAnimal = PetShop.Programare.idAnimal", Programare.class)
				.getResultList();
		List<Programare> results = entityManager.createNativeQuery("Select * from PetShop.Animal, PetShop.Programare where PetShop.Animal.idAnimal = PetShop.Programare.idAnimal", Programare.class)
				.getResultList();
		for(Programare prog : results) {
			Instant instant = prog.getData().toInstant();
			localDate.add(instant.atZone(ZoneId.systemDefault()).toLocalDate());
		}
		return localDate;
	}
}
