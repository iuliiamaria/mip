package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.DatabaseGenerics;
import util.DatabaseUtil;

public class Main extends Application {
		@Override
		/**
		 * Function that opens the GUI with LogIn.fxml
		 */
		public void start(Stage primaryStage) throws Exception{
			FXMLLoader loader = new FXMLLoader(this.getClass().getResource("LogIn.fxml"));
			Parent parent = loader.load();
			
			Scene scene = new Scene(parent);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Log In");
			primaryStage.show();
		}

	public static void main(String[] args) throws Exception {
		
		DatabaseGenerics<DatabaseUtil> db = DatabaseGenerics.getInstance();
		
		launch(args);
		
		db.closeEntity();
	}
}