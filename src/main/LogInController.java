package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LogInController implements Initializable {

	@FXML private TextField user;
	@FXML private PasswordField password;
	@FXML private Button logInButton;
	@FXML private Button exitButton;
	
	private Socket socket;
    private BufferedReader serverResponse;
    private PrintWriter loginDataProvider;
 
    private static final String loginAccepted = "Access granted";
	
	@Override
	/**
	 * Function that makes the connection with the server, 
	 * and if the data provided is correct then DoctorsView.fxml is launched,
	 * else we get an alert saying that the data provided is wrong
	 */
	public void initialize(URL location, ResourceBundle resources) {
		
		//try the connection to the server
		try {
            socket = new Socket("localhost", 5000);
            serverResponse = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            loginDataProvider =  new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Can't connect to server.");
        }
		
		//when the Log in button is clicked verify if the user name and password are the same as the ones known by the server
		logInButton.setOnMouseClicked(event -> {
			if (password.getText() != null && user.getText() != null) {
					
			String username = user.getText();
			String pass = password.getText();
			
			loginDataProvider.println(username);
			loginDataProvider.println(pass);
			
			//try creating a new window
			try {
				//if the user name and password are correct, make a new window that opens DoctorsView.fxml
				if(serverResponse.readLine().equals(loginAccepted)) {
				Scene currentScene=logInButton.getScene();
				currentScene.getWindow().hide();
							
				FXMLLoader fxmlLoader = new FXMLLoader();
				fxmlLoader.setLocation(getClass().getResource("DoctorsView.fxml"));
				Scene scene = new Scene(fxmlLoader.load());
				Stage stage = new Stage();
				stage.setTitle("Pet Shop");
				stage.setScene(scene);
				stage.show();
				}
			else {
				//else pop an alert that tells that the user name and password are incorrect
				Alert alert = new Alert(Alert.AlertType.ERROR);
		        alert.setHeaderText("Wrong username or password!");
		        alert.setContentText("Please try again.");
		 
		        alert.showAndWait();
				}
					    
			} catch (IOException e) {
				//if the new window is not created, show a message
				System.out.println("Failed to create a new window.");
				}
			}
			});
		
		//exit button
		exitButton.setOnAction(event -> Platform.exit());
	}
}