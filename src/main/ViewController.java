package main;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.util.Callback;
import model.Programare;
import util.AnimalUtil;
import util.ProgramareUtil;

public class ViewController implements Initializable {
	private ProgramareUtil dbP = new ProgramareUtil();
	private AnimalUtil dbA = new AnimalUtil();
	int idCurrentAnimal;

	@FXML private ListView<Programare> listProg = new ListView<>();
	@FXML private ListView<String> listIstoric = new ListView<String>();
	@FXML private TextField obs;
	@FXML private TextField specie;
	@FXML private TextField animal;
	@FXML private TextField stapan;
	@FXML private TextField varsta;
	@FXML private DatePicker data;
	@FXML private ImageView poza;
	@FXML private Button redBttn;
	@FXML private Button cancelBttn;
	@FXML private Button updateBttn;
	
	private ObservableList<Programare> lista = FXCollections.observableArrayList();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// exit button
		redBttn.setOnAction(event -> Platform.exit());
		// cancel an appointment by removing it from the list
		cancelBttn.setOnAction(event -> {
			Programare item = listProg.getSelectionModel().getSelectedItem();
			listProg.getItems().remove(item);
		});
		// add observations to the animal by updating the table Animal
		updateBttn.setOnAction(event -> {
			String myObs;
			myObs = obs.getText();
			dbA.updateObservatii(myObs, idCurrentAnimal);
		});
		
		// setting the DatePicker to get the current date
		data.setValue(LocalDate.now()); 
		// custom day cells of the date picker
	    data.setDayCellFactory(dayCellFactory);
		
	    // listener that based on the id of the appointment shows all the data related to it
		listProg.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			specie.setText(newValue.getAnimal().getSpecies());
			animal.setText(newValue.getAnimal().getName());
			stapan.setText(newValue.getAnimal().getOwner());
			varsta.setText(newValue.getAnimal().getAge());
			poza.setImage(dbA.listImages(newValue));
			listIstoric.getItems().setAll(newValue.getAnimal().getIstoric().split(","));
			idCurrentAnimal = newValue.getAnimal().getIdAnimal();
		});
}

	/**
	 * Function that lists the appointments with the text "Programare" followed by the appointment's id
	 */
static class ProgramariCell extends ListCell<Programare> {
	@Override
	protected void updateItem(Programare item, boolean bln) {
		super.updateItem(item, bln);
		if (item != null)
			setText("Programare " + item.getIdProgramare());
	    }
	}

/**
 * Function that colors the cells from the DatePicker if there is an appointment on that day,
 * and lists only the appointments from the selected date in the ListView when we click on the colored cell
 */
final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
	@Override
	public DateCell call(final DatePicker datePicker) {
		return new DateCell() {
			@Override
			public void updateItem(LocalDate item, boolean empty) {
				super.updateItem(item, empty);
				
				if (dbP.listDates().contains(item)) {
					setStyle("-fx-background-color: #ffc0cb;");
					
					data.setOnAction(event -> {
						// remove all the appointments from a previous date in order to show only the appointment from the selected day
						lista.removeAll(dbP.listAllAppointmentsSortedByDate());
						// add all the appointments to the ObservableList
                    	lista.addAll(dbP.listAllAppointments(data));
                    	// set the ListView with the items from the ObservableList
                    	listProg.setItems(lista);
                    	// custom cells of the ListView
                    	listProg.setCellFactory(list -> new ProgramariCell());
                    	});
					}
				}
			};
			}
	};
}